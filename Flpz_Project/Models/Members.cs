﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace Flpz_Project.Models
{
    public class Members
    {
        [Key]
        public int Id { get; set; }

        [StringLength(20)]
        public string Flp_username { get; set; }
        [StringLength(20)]
        public string Flp_password { get; set; }
        [StringLength(50)]
        public string Flp_name { get; set; }
        [StringLength(50)]
        public string Flp_surname { get; set; }
        public string Flp_emailaddress { get; set; }
        public string Flp_address { get; set; }
        public string Flp_deliveryaddress { get; set; }
        public string Flp_region { get; set; }
        public string Flp_postcode { get; set; }
        public string Flp_cellularnumber { get; set; }
        public string Flp_homephone { get; set; }
        public string Flp_workphone { get; set; }
        public string Flp_gender { get; set; }
        public string Flp_dateofbirth { get; set; }
        public string Flp_occupation { get; set; }
        public string Flp_country { get; set; }
        public string Flp_city { get; set; }
        public DateTime Flp_reg_date { get; set; }
        public string Flp_sponsor { get; set; }
        public string Flp_position { get; set; }
        public DateTime Flp_lastaccess { get; set; }
        public bool Approved { get; set; }
        public bool Disabled { get; set; }
        public bool Bullettin { get; set; }
        public string Activate_code { get; set; }
        public bool Flp_emailconfirmed { get; set; }
        public string Flp_appadministrator { get; set; }
        public string Flp_phonemodel { get; set; }
        public int Flp_modeladded { get; set; }
        public int Flp_download { get; set; }
        public string Flp_phonemodelgroup { get; set; }
        public string Flp_firstpay { get; set; }
        public bool Flp_bullettin_confirmed { get; set; }
        public bool Flp_isndp { get; set; }
        public bool Flp_emailconf2012 { get; set; }
        public string MiddleName { get; set; }
        public string Tcno { get; set; }
        public string Married { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseMiddleName { get; set; }
        public string SpouseLastName { get; set; }
        public string Bankname { get; set; }
        public string Branchname { get; set; }
        public string Branchcode { get; set; }
        public string Accno { get; set; }
        public string Iban { get; set; }
        public bool Flp_onlinereg { get; set; }
        public bool Flp_onlineapp { get; set; }
        public string Flp_lang { get; set; }
        public string Passportno { get; set; }
        public bool Flp_agree { get; set; }
    }
}
