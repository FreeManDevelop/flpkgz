﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flpz_Project.Models
{
    public class Region
    {
        public int Id { get; set; }
        public string RegionName { get; set; }
    }
}
