﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Flpz_Project.Models.AccountViewModels
{
    public class PasswordRecoveryViewModel
    {
        [Remote(action:"TrueOrFalseEmail",controller:"Account",ErrorMessage = "К сожалению такая почта не существует в базе")]
        public string Email { get; set; }
    }
}
