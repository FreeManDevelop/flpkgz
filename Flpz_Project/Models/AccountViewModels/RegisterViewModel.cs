﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Flpz_Project.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        public string IdNumber { get; set; }
        public string IdNumber2 { get; set; }
        public string IdNumber3 { get; set; }
        public string IdNumber4 { get; set; }
        public string Rank { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Iin { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Occupation { get; set; }
        public string Email { get; set; }
        public string EmailConfirm { get; set; }
        public bool Bullettin { get; set; }
        public string Address { get; set; }
        public string AddressDelivery { get; set; }
        public int States { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string HomephoneNumber { get; set; }
        public string HomephoneNumber2 { get; set; }
        public string WorkphoneNumber { get; set; }
        public string WorkphoneNumber2 { get; set; }
        public string CellphoneNumber { get; set; }
        public string CellphoneNumber2 { get; set; }
        public string SponsorName { get; set; }
        public int Ydfstatus { get; set; }
        public int Sid { get; set; }
    }
}
