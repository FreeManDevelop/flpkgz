﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Flpz_Project.Models.AccountViewModels
{
    public class JoinViewModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int BirthDay { get; set; }
        public int BirthMonth { get; set; }
        public int BirthYear { get; set; }
        public string PassportID { get; set; }
        public string PrimaryIIN { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public int States { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Postal { get; set; }
        public string PhoneNumber { get; set; }
        public string AltPhone { get; set; }
        [Remote(action:"IsEmailAddress", controller:"Account", ErrorMessage = "Такая почта уже зарегистрирована")]
        public string Email { get; set; }
        public string EmailConfirm { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        [Remote(action: "IsDistId", controller: "Account", ErrorMessage = "Такой пользователь уже зарегистрирован")]
        public string DistId { get; set; }
        public string SponsorId { get; set; }
        public bool AcceptTermsAndConditions { get; set; }
        public bool Acceptsales { get; set; }
    }
}
