﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flpz_Project.Models.AccountViewModels
{
    public class LoginViewModel
    {
        public string LogonId { get; set; }
        public string LogonPassword { get; set; }
    }
}
