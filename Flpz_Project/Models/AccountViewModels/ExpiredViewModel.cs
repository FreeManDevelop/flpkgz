﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flpz_Project.Models.AccountViewModels
{
    public class ExpiredViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string WarningMessage { get; set; }
        public string LinkMessage { get; set; }
    }
}
