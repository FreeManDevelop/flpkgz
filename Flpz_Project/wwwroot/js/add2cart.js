// JavaScript Document
var Achtung = function(msg){
 var achtung = $('<div id="achtung">'+msg+'<span class="close">x</span></div>');
 $(achtung)
  .css('position', 'fixed')
  .css('top', '5px')
  //.css('left', '50%')
  .css('display', 'none')
  .css('align','center')
  .css('width', '100%')
  .css('margin-left', 'auto')
  .css('margin-right', 'auto');
 $('body').append(achtung);
 $(achtung).fadeIn(1500);
 var hideTimer = setInterval(hideAchtung, 3000);
 //$('#achtung .close').click(function(){hideAchtung();});
 $(document).on('click','#achtung .close',function(){hideAchtung();});
}

//var hideAchtung = function(){ $('#achtung').fadeOut(500); $('#achtung').remove(); }
var hideAchtung = function(){ $('#achtung').fadeOut(500, function(){$('#achtung').remove();}); }

function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function sendcart(qty, prodID, cart_count) {		
		
		var strURL="/inc/add2cart.asp?qty="+qty+"&prodID="+prodID+"&cart_count="+cart_count;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('cart_count').innerHTML=req.responseText;
						redrawcontent();
						Achtung('Товар добавлен в корзину.');
					} else {
						alert("XMLHTTP hatasi olustu:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	}
	
	function redrawcontent() {		
		
		var strURL="/inc/cart_content.asp?"+Math.random();
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('forre-request').innerHTML=req.responseText;				
					} else {
						alert("XMLHTTP hatasi olustu:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	}