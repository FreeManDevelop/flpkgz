
jQuery().ready(function($) {
	$('.spinnerfast').spinner({ min: 1, max: 100, increment: 'fast' });
});

$(document).ready(function() {

	//Default Action
	avisa_form_validator($('#anket'));
		
	$('#anket').submit(function(e){
		var v = $('#anket .has-error:first');
        if (v.length > 0) alert('Пожалуйста, заполните все поля');
		});
		
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass('active').show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
		
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass('active'); //Remove any "active" class
		$(this).addClass('active'); //Add "active" class to selected tab
			
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).parents('li').find('a').attr('href'); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
		
							
	// Slideshow 1
	$("#slider1").responsiveSlides({
	auto: true,
	pager: true,
	nav: true,
	speed: 500,
	namespace: "centered-btns"
	});

	$(function () {
	$('.small-menu').hide();
	$('.show-small-menu').click(function (e) {
        var $message = $('.small-menu');
        
		if ($message.css('display') !== 'block') {
		$message.show();
		var yourClick = true;
		$(document).bind('click.myEvent', function (e) {
			if (!yourClick && $(e.target).closest('.small-menu').length === 0) {
			$message.hide();
			$(document).unbind('click.myEvent');
			}
			yourClick = false;
		});
		}
		e.preventDefault();
	});
	});

	$(function () {
	$('.st-accordion').hide();
	$('.main-menu-small-btn').click(function (e) {
		$(this).addClass('active');
		var $message = $('.st-accordion');
		if ($message.css('display') !== 'block') {
		$message.show();
		var yourClick = true;
		$(document).bind('click.myEvent', function (e) {

			if (!yourClick && $(e.target).closest('.st-accordion').length === 0) {
			$message.hide();
			$('.main-menu-small-btn').removeClass('active');
			$(document).unbind('click.myEvent');
			}
			yourClick = false;
		});
		}
		e.preventDefault();
	});
	});

});

$(function() {
				
	$('#st-accordion').accordion({
		oneOpenedItem	: true
	});
	$('.anket-accordeon').accordion({
		oneOpenedItem	: true,
		open: 0
	});
		
});

(function($) {
    $(function() {
        $('.selectbox').selectbox();
        $('input.radio').radio();
    });
});
	
function getXMLHTTP() { //fuction to return the xml http object
var xmlhttp=false;	
try{
	xmlhttp=new XMLHttpRequest();
}
catch(e){		
	try{			
		xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
	}
	catch(e){
		try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e1){
			xmlhttp=false;
		}
	}
}
return xmlhttp;
}

function getState(countryId) {
    debugger;
    var strURL ="GetState?regionId="+countryId+"&rnd="+Math.random();
	var req = getXMLHTTP();
	var selects = document.getElementById("anket");
	var answer = selects.states.options[selects.states.selectedIndex].text;
	selects.state.value = answer;
	

	if (req) {
		req.onreadystatechange = function() {
			if (req.readyState === 4) {
				// only if "OK"
				if (req.status === 200) {						
					document.getElementById('statediv').innerHTML=req.responseText;
					$('.city-select').selectbox();						
				} else {
					alert("XMLHTTP ошибка:\n" + req.statusText);
				}
			}				
		}			
		req.open("GET", strURL, true);
		
		req.send(null);
	}		
}

function trigAcc(x){
   $(".anket-accordeon li > a:eq("+ x +")").trigger('click');
};

//Function on Login

function trim(sString) {
    sTrimmedString = "";
    if (sString !== "") {
        var iStart = 0;
        var iEnd = sString.length - 1;
        var sWhitespace = " \t\f\n\r\v";

        while (sWhitespace.indexOf(sString.charAt(iStart)) !== -1) {
            iStart++;
            if (iStart > iEnd)
                break;
        }

        if (iStart <= iEnd) {
            while (sWhitespace.indexOf(sString.charAt(iEnd)) !== -1)
                iEnd--;
            sTrimmedString = sString.substring(iStart, ++iEnd);
        }
    }
    return sTrimmedString;
}

function prepareSubmit(form) {
    var bSub = true;

    if (trim(form.logonId.value) === "") {
        alert("Введите свой ID номер");
        bSub = false;
        form.logonId.value = "";
        form.logonId.select();
        return false;
    }

    if (form.logonId.value.length !== 12) {
        alert("ID номер должен состоять из 12 цифр");
        bSub = false;
        form.logonId.focus();
        form.logonId.select();
        return false;
    }


    if (!validNum(form.logonId)) {
        alert("Введите свой ID номер");
        bSub = false;
        form.logonId.select();
        return false;
    }

    if (trim(form.logonPassword.value) === "") {
        alert("Введите свой пароль");
        bSub = false;
        form.logonPassword.value = "";
        form.logonPassword.select();
        return false;
    }

    if (bSub) {
        form.submit();
    }
}
// For confirm with conditions
function validsend(form) {

    if (!form.acceptTermsAndConditions.checked) {
        alert("Для продолжения вы должны принять условия и правила.");
        return false;
    }

    if (!form.acceptsales.checked) {
        alert("Для продолжения вы должны принять условия договора купли и продажи.");
        return false;
    }

    form.submit();
}