jQuery().ready(function($) {
			$('.spinnerfast').spinner({ min: 1, max: 100, increment: 'fast' });
		});


		$(document).ready(function() {

			//Default Action
			$(".tab_content").hide(); //Hide all content
			$("ul.tabs li:first").addClass("active").show(); //Activate first tab
			$(".tab_content:first").show(); //Show first tab content

			//On Click Event
			$("ul.tabs li").click(function() {
				$("ul.tabs li").removeClass("active"); //Remove any "active" class
				$(this).addClass("active"); //Add "active" class to selected tab
				$(".tab_content").hide(); //Hide all tab content
				var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
				$(activeTab).fadeIn(); //Fade in the active content
				return false;
			});
			// Slideshow 1
			  $("#slider1").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 500,
				namespace: "centered-btns"
			  });

			 $(function () {
				$('.small-menu').hide();
				$('.show-small-menu').click(function (e) {
				  var $message = $('.small-menu');
			 
				  if ($message.css('display') != 'block') {
					$message.show();
			 
					var yourClick = true;
					$(document).bind('click.myEvent', function (e) {
					  if (!yourClick && $(e.target).closest('.small-menu').length == 0) {
						$message.hide();
						$(document).unbind('click.myEvent');
					  }
					  yourClick = false;
					});
				  }
			 
				  e.preventDefault();
				});
			  });

			 $(function () {
				$('.st-accordion').hide();
				$('.main-menu-small-btn').click(function (e) {
				  var $message = $('.st-accordion');
			 
				  if ($message.css('display') != 'block') {
					$message.show();
			 
					var yourClick = true;
					$(document).bind('click.myEvent', function (e) {
					  if (!yourClick && $(e.target).closest('.st-accordion').length == 0) {
						$message.hide();
						$(document).unbind('click.myEvent');
					  }
					  yourClick = false;
					});
				  }
			 
				  e.preventDefault();
				});
			  });

		});

		 $(function() {
					
						$('#st-accordion').accordion({
							oneOpenedItem	: true
						});
						
					});