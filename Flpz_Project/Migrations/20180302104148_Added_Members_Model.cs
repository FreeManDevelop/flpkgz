﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Flpz_Project.Migrations
{
    public partial class Added_Members_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Memberses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Accno = table.Column<string>(nullable: true),
                    Activate_code = table.Column<string>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    Bankname = table.Column<string>(nullable: true),
                    Branchcode = table.Column<string>(nullable: true),
                    Branchname = table.Column<string>(nullable: true),
                    Bullettin = table.Column<bool>(nullable: false),
                    Disabled = table.Column<bool>(nullable: false),
                    Flp_address = table.Column<string>(nullable: true),
                    Flp_agree = table.Column<bool>(nullable: false),
                    Flp_appadministrator = table.Column<string>(nullable: true),
                    Flp_bullettin_confirmed = table.Column<bool>(nullable: false),
                    Flp_cellularnumber = table.Column<string>(nullable: true),
                    Flp_city = table.Column<string>(nullable: true),
                    Flp_country = table.Column<string>(nullable: true),
                    Flp_dateofbirth = table.Column<string>(nullable: true),
                    Flp_deliveryaddress = table.Column<string>(nullable: true),
                    Flp_download = table.Column<int>(nullable: false),
                    Flp_emailaddress = table.Column<string>(nullable: true),
                    Flp_emailconf2012 = table.Column<bool>(nullable: false),
                    Flp_emailconfirmed = table.Column<bool>(nullable: false),
                    Flp_firstpay = table.Column<string>(nullable: true),
                    Flp_gender = table.Column<string>(nullable: true),
                    Flp_homephone = table.Column<string>(nullable: true),
                    Flp_isndp = table.Column<bool>(nullable: false),
                    Flp_lang = table.Column<string>(nullable: true),
                    Flp_lastaccess = table.Column<DateTime>(nullable: false),
                    Flp_modeladded = table.Column<int>(nullable: false),
                    Flp_name = table.Column<string>(maxLength: 50, nullable: true),
                    Flp_occupation = table.Column<string>(nullable: true),
                    Flp_onlineapp = table.Column<bool>(nullable: false),
                    Flp_onlinereg = table.Column<bool>(nullable: false),
                    Flp_password = table.Column<string>(maxLength: 20, nullable: true),
                    Flp_phonemodel = table.Column<string>(nullable: true),
                    Flp_phonemodelgroup = table.Column<string>(nullable: true),
                    Flp_position = table.Column<string>(nullable: true),
                    Flp_postcode = table.Column<string>(nullable: true),
                    Flp_reg_date = table.Column<DateTime>(nullable: false),
                    Flp_region = table.Column<string>(nullable: true),
                    Flp_sponsor = table.Column<string>(nullable: true),
                    Flp_surname = table.Column<string>(maxLength: 50, nullable: true),
                    Flp_username = table.Column<string>(maxLength: 20, nullable: true),
                    Flp_workphone = table.Column<string>(nullable: true),
                    Iban = table.Column<string>(nullable: true),
                    Married = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Passportno = table.Column<string>(nullable: true),
                    SpouseFirstName = table.Column<string>(nullable: true),
                    SpouseLastName = table.Column<string>(nullable: true),
                    SpouseMiddleName = table.Column<string>(nullable: true),
                    Tcno = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Memberses", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Memberses");
        }
    }
}
