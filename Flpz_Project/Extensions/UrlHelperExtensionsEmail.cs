﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flpz_Project.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Flpz_Project.Extensions
{
    public static class UrlHelperExtensionsEmail
    {
        public static string EmailConfirmationLink(this IUrlHelper urlHelper, DateTime timeCreateLink, string code, int userId, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ConfirmEmail),
                controller: "Account",
                values: new { timeCreateLink, code , userId },
                protocol: scheme);
        }
    }
}
