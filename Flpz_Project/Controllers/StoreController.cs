﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flpz_Project.Data;
using Flpz_Project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Flpz_Project.Controllers
{
    public class StoreController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StoreController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Profile()
        {
            int? userId = HttpContext.Session.GetInt32("userId");

            return View(userId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditeUserProfile(int userId)
        {
            Members member = _context.Memberses.FirstOrDefault(u => u.Id == userId);
            ViewBag.Region = new SelectList(_context.Regions, "Id", "RegionName");
            return View(member);
        }

        [HttpGet]
        public IActionResult GetState(int regionId)
        {
            IEnumerable<City> citys = _context.Cities.Where(c => c.RegionId == regionId);
            string xmlstring = "<select name=\"city\" >";
            foreach (var c in citys)
            {
                xmlstring += "<option value = " + c.CityName + " \">" + c.CityName + "</option>";
            }
            xmlstring += "</select>";

            return new ContentResult
            {
                ContentType = "application/xml",
                Content = xmlstring,
                StatusCode = 200
            };
        }
    }
}