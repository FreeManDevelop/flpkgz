﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Flpz_Project.Data;
using Flpz_Project.Extensions;
using Flpz_Project.Models;
using Flpz_Project.Models.AccountViewModels;
using Flpz_Project.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Flpz_Project.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IEmailSender _sender;

        public AccountController(ApplicationDbContext context, IEmailSender sender)
        {
            _context = context;
            _sender = sender;
        }

        public IActionResult Select()
        {
            return View();
        }

        public IActionResult Joins()
        {
            ViewBag.Region = new SelectList(_context.Regions, "Id", "RegionName");

           return View();
        }

        public IActionResult Register()
        {
            ViewBag.Region = new SelectList(_context.Regions, "Id", "RegionName");
            return View();
        }

        [HttpGet]
        public IActionResult Login() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Joins(JoinViewModel model, string secondProcessing)
        {
            if (string.IsNullOrEmpty(secondProcessing))
            {
                return View("Check", model);
            }
            else
            {
                Region region = _context.Regions.FirstOrDefault(r => r.Id == model.States);
                string gndCodeForConfirm = Guid.NewGuid().ToString();
                string gndPassword = GndPassword();
                DateTime dateLimit = DateTime.Now.AddHours(72);

                _context.Memberses.Add(new Members
                {
                    Activate_code = gndCodeForConfirm,
                    Flp_city = model.City,
                    Flp_password = gndPassword,
                    Flp_region = region.RegionName,
                    Flp_reg_date = DateTime.Now,
                    Flp_dateofbirth = model.BirthDay + "/" + model.BirthMonth + "/" + model.BirthYear,
                    Flp_username = model.DistId,
                    Flp_emailaddress = model.Email,
                    Flp_emailconfirmed = false,
                    Flp_name = model.FirstName,
                    Flp_postcode = model.Postal,
                    Flp_address = model.Address1,
                    Flp_bullettin_confirmed = false,
                    Flp_surname = model.LastName,
                    Flp_sponsor = model.SponsorId,
                    Flp_cellularnumber = model.PhoneNumber,
                    Flp_homephone = model.AltPhone,
                    Flp_workphone = model.PhoneNumber,
                    Flp_country = model.Country,
                    Bankname = model.Custom1,
                    Flp_deliveryaddress = model.Address1,
                    Approved = true,
                    MiddleName = model.MiddleName,
                    Tcno = model.PrimaryIIN,
                    Passportno = model.PassportID,
                    Branchname = model.Custom2,
                    Iban = model.Custom3,
                    Flp_onlinereg = true
                });
                _context.SaveChanges();
                Members user = _context.Memberses.FirstOrDefault(u => u.Activate_code == gndCodeForConfirm);
                var callbackUrl = Url.EmailConfirmationLink(dateLimit, gndCodeForConfirm, user.Id, Request.Scheme);
                await _sender.SendEmailConfirmationAsync(callbackUrl,model,gndPassword);

                return View("CheckEmail", model.Email);
            }
        }


        public  IActionResult ConfirmEmail(DateTime timeCreateLink, string code, int userId)
        {
            Members member = _context.Memberses.FirstOrDefault(m => m.Id == userId);

            if (member != null)
            {
                if (timeCreateLink >= DateTime.Now)
                {
                    if (code == member.Activate_code)
                    {
                        member.Flp_emailconfirmed = true;
                        _context.Memberses.Update(member);
                        _context.SaveChanges();
                        return View(member);
                    }
                    else
                    {
                        return View("Expired", new ExpiredViewModel
                        {
                            Title = "Код активации не действительна",
                            Body = "Активационый код был изменнен либо получен не правильный код активации!"
                        });
                    }
                }
                else
                {
                    _context.Memberses.Remove(member);
                    _context.SaveChanges();

                    return View("Expired", new ExpiredViewModel
                    {
                        Title = "Cрок действия ссылки истек",
                        Body = "Пожалуйста пройдите регистрацию заного и активируйте аккаунт до истечения действия ссылки",
                        WarningMessage = "(ссылка дейтвует 72 часа с момента отправки её на почту!)"
                    });
                }
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            IEnumerable<Members> members = _context.Memberses;
            if (members.FirstOrDefault(m =>
                    m.Flp_username == model.IdNumber + model.IdNumber2 + model.IdNumber3 + model.IdNumber4 || m.Flp_emailaddress.Trim().ToLower() == model.Email.Trim().ToLower()) != null)
            {
                return View("Expired", new ExpiredViewModel
                {
                    Title = "Такой пользователь уже зарегистрирован",
                    Body = $"Ваш ID номер ({model.IdNumber + model.IdNumber2 + model.IdNumber3 + model.IdNumber4}) и/или адрес электронной почты ({model.Email}) уже зарегистрированы в системе.",
                    LinkMessage = "Not Null"
                });
            }

            Region region = _context.Regions.FirstOrDefault(r => r.Id == model.States);
            string gndCodeForConfirm = Guid.NewGuid().ToString();
            string gndPassword = GndPassword();
            DateTime dateLimit = DateTime.Now.AddHours(72);

            _context.Memberses.Add(new Members
            {
                Flp_username = model.IdNumber + model.IdNumber2 + model.IdNumber3 + model.IdNumber4,
                Activate_code = gndCodeForConfirm,
                Flp_region = region.RegionName,
                Flp_password = gndPassword,
                Flp_name = model.FirstName,
                Flp_surname = model.LastName,
                Flp_sponsor = model.SponsorName,
                Flp_emailaddress = model.Email,
                Flp_country = model.Country,
                Flp_postcode = model.PostCode,
                Flp_bullettin_confirmed = model.Bullettin,
                Flp_address = model.Address,
                Flp_deliveryaddress = model.AddressDelivery,
                Flp_city = model.City,
                Tcno = model.Iin,
                Flp_occupation = model.Occupation,
                Flp_homephone = model.HomephoneNumber + model.HomephoneNumber2,
                Flp_workphone = model.WorkphoneNumber + model.WorkphoneNumber2,
                Flp_cellularnumber = model.CellphoneNumber + model.CellphoneNumber2,
                Flp_reg_date = DateTime.Now,
                Flp_position = model.Rank,
                Flp_dateofbirth = model.Day+"/"+model.Month+"/"+model.Year
            });
            _context.SaveChanges();
            Members user = _context.Memberses.FirstOrDefault(u => u.Activate_code == gndCodeForConfirm);
            var callbackUrl = Url.EmailConfirmationLink(dateLimit, gndCodeForConfirm, user.Id, Request.Scheme);
            await _sender.SendEmailConfirmationForDestributerAsync(callbackUrl, model, gndPassword);

            return View("CheckEmail", model.Email);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel model)
        {
            Members members = _context.Memberses.FirstOrDefault(m => m.Flp_username == model.LogonId);
            if (members != null)
            {
                if (members.Flp_password == model.LogonPassword)
                {
                    HttpContext.Session.SetInt32("userId", members.Id);

                    return RedirectToAction(controllerName: "Store", actionName: "Index");
                }
                ModelState.AddModelError(String.Empty, "Неправильный пароль и (или) логин!");
                return View(model);
            }
            else {
                ModelState.AddModelError(String.Empty, "Неправильный пароль и (или) логин!");
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult GetState(int regionId)
        {
            IEnumerable<City> citys = _context.Cities.Where(c => c.RegionId == regionId);
            string xmlstring = "<select name=\"city\" >";
            foreach (var c in citys)
            {
                xmlstring += "<option value = " + c.CityName + " \">"+c.CityName+ "</option>";
            }
            xmlstring += "</select>";

            return new ContentResult
            {
                ContentType = "application/xml",
                Content = xmlstring,
                StatusCode = 200
            };
        }

        #region CONDITIONS

        public IActionResult Terms() => View();
        public IActionResult Sales_Agreement() => View();

        #endregion

        #region Download

        //This is method for show pdf file -- warning this is method input only pdf File, dont input other file type
        public ActionResult GetPdf(string folder,string secondFolder,string fileName)
        {
            var fileStream = new FileStream("wwwroot/"+folder+"/"+secondFolder+"/" + fileName,
                FileMode.Open,
                FileAccess.Read
            );
            var fsResult = new FileStreamResult(fileStream, "application/pdf");
            return fsResult;
        }

        //This is method for download files
        //Downloading comes from the directory wwwroot/filname : example server.com/account/download/file.type
        //Todo this method need in upgrade
        //after upgrade this method be put value folder and fileName this for dinamic
        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        //Types whats can download method "Download"
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".zip", "application/zip"}
            };
        }
        #endregion

        #region Helpers

        private string GndPassword()
        {
            Random rnd = new Random();
            string passValue = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string resultPass = string.Empty;

            for (int i = 0; i < 8; i++)
            {
                resultPass += passValue[rnd.Next(0, passValue.Length)];
            }
            return resultPass;
        }

        #endregion

        #region For password recovery   

        public IActionResult PasswordRecovery() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PasswordRecovery(PasswordRecoveryViewModel model)
        {
            Members member = _context.Memberses.FirstOrDefault(m => m.Flp_emailaddress.ToLower() == model.Email.Trim().ToLower());
            await _sender.SendPasswordRecoveryAsync(member.Flp_emailaddress, member.Flp_password);
            return View("RecoveryPasswordSend", member.Flp_emailaddress);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult TrueOrFalseEmail(string email)
        {
            Members member = _context.Memberses.FirstOrDefault(m => m.Flp_emailaddress.ToLower() == email.Trim().ToLower());

            return Json(member != null);
        }

        #endregion

        #region Validation

        [AcceptVerbs("GET","POST")]
        public IActionResult IsEmailAddress(string email)
        {
            Members member = _context.Memberses.FirstOrDefault(m => m.Flp_emailaddress == email);
            return Json(member == null);
        }

        [AcceptVerbs("GET","POST")]
        public IActionResult IsDistId(string distId)
        {
            Members member = _context.Memberses.FirstOrDefault(m => m.Flp_username == distId);
            return Json(member == null);
        }

        #endregion
    }
}