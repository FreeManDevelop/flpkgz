﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Flpz_Project.Models;
using Microsoft.AspNetCore.Http;

namespace Flpz_Project.Controllers
{
    public class HomeController : Controller
    { 
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult About()
        {
            return Redirect("http://foreverliving.com/page/journey/kaz/ru");
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Facebook() => Redirect("https://www.facebook.com/foreverkazakhstanhq");

        public IActionResult OfficeInWorld() => Redirect("http://foreverliving.com/page/home-page/locations/kaz/ru");

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
