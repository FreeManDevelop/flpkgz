﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flpz_Project.Data;
using Flpz_Project.Models;

namespace Flpz_Project.InitializeData
{
    public static class InitializeDataClass
    {
        public static void Initialize(ApplicationDbContext _context)
        {
            if (!_context.Regions.Any())
            {
                _context.Regions.AddRange(new List<Region>
                {
                    new Region
                    {
                        RegionName = "Выберите область"
                    },
                    new Region
                    {
                        RegionName = "Баткенская область"
                    },
                    new Region
                    {
                        RegionName = "Джалал-Абадская область"
                    },
                    new Region
                    {
                        RegionName = "Иссык-Кульская область"
                    },
                    new Region
                    {
                        RegionName = "Нарынская область"
                    },
                    new Region
                    {
                        RegionName = "Ошская область"
                    },
                    new Region
                    {
                        RegionName = "Таласская область"
                    },
                    new Region
                    {
                        RegionName = "Чуйская область"
                    }
                });
                _context.SaveChanges();
            }

            if (!_context.Cities.Any() && _context.Regions.Any())
            {
                IEnumerable<Region> regions = _context.Regions;
                int id1 = regions.FirstOrDefault(r => r.RegionName == "Баткенская область").Id;
                int id2 = regions.FirstOrDefault(r => r.RegionName == "Джалал-Абадская область").Id;
                int id3 = regions.FirstOrDefault(r => r.RegionName == "Иссык-Кульская область").Id;
                int id4 = regions.FirstOrDefault(r => r.RegionName == "Нарынская область").Id;
                int id5 = regions.FirstOrDefault(r => r.RegionName == "Ошская область").Id;
                int id6 = regions.FirstOrDefault(r => r.RegionName == "Таласская область").Id;
                int id7 = regions.FirstOrDefault(r => r.RegionName == "Чуйская область").Id;

                _context.Cities.AddRange(new List<City>
                {
                    new City
                    {
                        RegionId = id1,
                        CityName = "город Баткен"
                    },
                    new City
                    {
                        RegionId = id1,
                        CityName = "город Кызыл-Кия"
                    },
                    new City
                    {
                        RegionId = id1,
                        CityName = "город Сулюкта"
                    },
                    new City
                    {
                        RegionId = id1,
                        CityName = "Баткенский район"
                    },
                    new City
                    {
                        RegionId = id1,
                        CityName = "Кадамжайский район"
                    },
                    new City
                    {
                        RegionId = id1,
                        CityName = "Лейлекский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "город Джалал-Абад"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "город Кара-Куль"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "город Майлуу-Суу"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "город Таш-Кумыр"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Аксыйский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Ала-Букинский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Базар-Коргонский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Ноокенский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Сузакский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Тогуз-Тороуский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Токтогульский район"
                    },
                    new City
                    {
                        RegionId = id2,
                        CityName = "Чаткальский район"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "город Балыкчи"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "город Каракол"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "Ак-Суйский район"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "Джети-Огузский район"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "Иссык-Кульский район"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "Тонский район"
                    },
                    new City
                    {
                        RegionId = id3,
                        CityName = "Тюпский район"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "город Нарын"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "Ак-Талинский район"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "Ат-Башинский район"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "Жумгальский район"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "Кочкорский район"
                    },
                    new City
                    {
                        RegionId = id4,
                        CityName = "Нарынский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "город Ош"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Алайский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Араванский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Кара-Кульджинский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Кара-Сууский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Ноокатский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Узгенский район"
                    },
                    new City
                    {
                        RegionId = id5,
                        CityName = "Чон-Алайский район"
                    },
                    new City
                    {
                        RegionId = id6,
                        CityName = "город Талас"
                    },
                    new City
                    {
                        RegionId = id6,
                        CityName = "Бакай-Атинский район"
                    },
                    new City
                    {
                        RegionId = id6,
                        CityName = "Кара-Бууринский район"
                    },
                    new City
                    {
                        RegionId = id6,
                        CityName = "Манасский район"
                    },
                    new City
                    {
                        RegionId = id6,
                        CityName = "Таласский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "город Токмак"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Аламудунский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Жайыльский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Кеминский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Московский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Панфиловский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Сокулукский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Чуйский район"
                    },
                    new City
                    {
                        RegionId = id7,
                        CityName = "Ысык-Атинский район"
                    }
                });
                _context.SaveChanges();
            }
        }
    }
}
